VERSION=0.0.1

default:
	hugo
	podman build -t codeberg.org/kallisti5/terarocket-site:${VERSION} .
clean:
	rm -rf public/
test:
	podman run --security-opt=seccomp=unconfined -P codeberg.org/kallisti5/terarocket-site:${VERSION}
push:
	podman push codeberg.org/kallisti5/terarocket-site:${VERSION}
